/**
 * --> Les classes que representan les begudes d'un bar
 *
 * Drink -> Sodas, Juices, AlcoholicDrinks
 * abstract class, herencia, interfaces
 *
 * (TBD) Al sistema de gestió de comandes d'un bar
 */

abstract class Drink{
    abstract val name: String
    abstract var price: Double

    open fun messageInfo() {
        println("You are drinking a ${this::class.qualifiedName} called $name that costs $price ")
    }
}

interface SugarTax{
    fun addSugarTax(price: Double): Double {
        return price/10
    }
}

interface InCocktail {
    fun modifyPriceWhenMixedWithAlcohol(): Double
}

class Juice(override val name: String, override var price: Double, val fruit: String): Drink() {

    init {
        super.messageInfo()
    }
}

class Soda(override val name: String, override var price: Double, val sparkles: Boolean): Drink(), SugarTax, InCocktail {
    init {
        price += addSugarTax(price)
    }

    override fun modifyPriceWhenMixedWithAlcohol(): Double {
        return price/2
    }

    override fun messageInfo() {
        super.messageInfo()
        println(if (sparkles) "This is a sparkled drink" else "This is not a sparkled drink")
    }
}

class AlcoholicDrink(override val name: String, override var price: Double, val graduation: Int, val mixedWith: Soda?): Drink(), SugarTax {
    init {
        price += addSugarTax(price)
        if (mixedWith != null) price += mixedWith.modifyPriceWhenMixedWithAlcohol()
    }

    override fun messageInfo() {
        super.messageInfo()
        println("The graduation of this drink is $graduation")
    }
}


fun main() {
    val coke = Soda("Coca-Cola", 1.95, true)
    val rumWithCoke = AlcoholicDrink("Cuba-libre", 10.0, 12, coke)
    val valencianito = Juice("Valencianito", 3.5, "Orange")

    coke.messageInfo()
    rumWithCoke.messageInfo()
    valencianito.messageInfo()
}
